
package it.polito.dp2.FDS.lab3.gen;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "InvalidFlightInstanceFault", targetNamespace = "http://pad.polito.it/dp2/FDSBooking")
public class InvalidFlightInstanceFault_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private InvalidFlightInstanceFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public InvalidFlightInstanceFault_Exception(String message, InvalidFlightInstanceFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public InvalidFlightInstanceFault_Exception(String message, InvalidFlightInstanceFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: it.polito.dp2.FDS.lab3.gen.InvalidFlightInstanceFault
     */
    public InvalidFlightInstanceFault getFaultInfo() {
        return faultInfo;
    }

}
