
package it.polito.dp2.FDS.lab3.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flightNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="flightDate" type="{http://pad.polito.it/dp2/FDSBooking}flightDateType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightNumber",
    "flightDate"
})
@XmlRootElement(name = "getPassengerList")
public class GetPassengerList {

    @XmlElement(required = true)
    protected String flightNumber;
    @XmlElement(required = true)
    protected FlightDateType flightDate;

    /**
     * Recupera il valore della proprietà flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Imposta il valore della proprietà flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Recupera il valore della proprietà flightDate.
     * 
     * @return
     *     possible object is
     *     {@link FlightDateType }
     *     
     */
    public FlightDateType getFlightDate() {
        return flightDate;
    }

    /**
     * Imposta il valore della proprietà flightDate.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightDateType }
     *     
     */
    public void setFlightDate(FlightDateType value) {
        this.flightDate = value;
    }

}
