
package it.polito.dp2.FDS.lab3.gen;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * A type that represents a flight instance date (implicitly assumed to be expressed in the Gregorian Calendar, in the time zone of the departure airport)
 * 
 * <p>Classe Java per flightDateType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="flightDateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="year" type="{http://pad.polito.it/dp2/FDSBooking}yearType"/>
 *         &lt;element name="month" type="{http://pad.polito.it/dp2/FDSBooking}monthType"/>
 *         &lt;element name="day" type="{http://pad.polito.it/dp2/FDSBooking}dayType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flightDateType", propOrder = {
    "year",
    "month",
    "day"
})
public class FlightDateType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger year;
    @XmlSchemaType(name = "positiveInteger")
    protected int month;
    @XmlSchemaType(name = "positiveInteger")
    protected int day;

    /**
     * Recupera il valore della proprietà year.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getYear() {
        return year;
    }

    /**
     * Imposta il valore della proprietà year.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setYear(BigInteger value) {
        this.year = value;
    }

    /**
     * Recupera il valore della proprietà month.
     * 
     */
    public int getMonth() {
        return month;
    }

    /**
     * Imposta il valore della proprietà month.
     * 
     */
    public void setMonth(int value) {
        this.month = value;
    }

    /**
     * Recupera il valore della proprietà day.
     * 
     */
    public int getDay() {
        return day;
    }

    /**
     * Imposta il valore della proprietà day.
     * 
     */
    public void setDay(int value) {
        this.day = value;
    }

}
