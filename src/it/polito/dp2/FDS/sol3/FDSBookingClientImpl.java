package it.polito.dp2.FDS.sol3;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.ws.Holder;

import com.sun.org.glassfish.external.probe.provider.PluginPoint;

import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.lab3.FDSBookingClient;
import it.polito.dp2.FDS.lab3.FDSUnbookingClient;
import it.polito.dp2.FDS.lab3.MissingDataException;
import it.polito.dp2.FDS.lab3.OperationFailException;
import it.polito.dp2.FDS.lab3.UnbookClientImpl;
import it.polito.dp2.FDS.lab3.gen.*;
import it.polito.dp2.FDS.lab3.server.FlightInstance;

public class FDSBookingClientImpl implements FDSBookingClient {
	private FDSBookingService _service;
	private FDSBooking _proxy;
	private String _number;
	private FlightDateType _depDate;
	private static final Logger LOGGER = Logger.getLogger(FDSBookingClientImpl.class.getName());
	
	/**
	 * @category Constructor
	 */
	public FDSBookingClientImpl() {
		URL serviceUrl;
		String urlString = System.getProperty("it.polito.dp2.FDS.sol3.URL");
		try {
			serviceUrl = new URL(urlString);
			_service = new FDSBookingService(serviceUrl);
		} catch (MalformedURLException e) {
			LOGGER.info("Selected url was null, trying to contact service at the default url");
			_service = new FDSBookingService();
		}
		_proxy = _service.getFDSBookingServiceSOAPPort();
	}
	
	/**
	 * @category Setter
	 */
	@Override
	public void setFlightNumber(String number) {
		_number = number;
	}

	/**
	 * @category Setter
	 */
	@Override
	public void setDepartureDate(GregorianCalendar gdate) {
		if(gdate == null) {
			LOGGER.info("Input date was null");
			return;
		}
		_depDate = new FlightDateType();
		_depDate.setDay(gdate.get(Calendar.DAY_OF_MONTH));
		_depDate.setMonth(gdate.get(Calendar.MONTH) + 1);
		_depDate.setYear(BigInteger.valueOf(gdate.get(Calendar.YEAR)));
	}

	/**
	 * @category Setter
	 */
	@Override
	public void setServiceURL(URL url) {
		if(url == null) {
			LOGGER.info("Selected url was null");
			return;
		}
		System.setProperty("it.polito.dp2.FDS.sol3.URL", url.toString());
		_service = new FDSBookingService(url);
		_proxy = _service.getFDSBookingServiceSOAPPort();
	}

	/**
	 * @category RPC
	 */
	@Override
	public Set<String> book(Set<String> passengerNames,
			boolean partialBookingAllowed) throws MissingDataException,
			OperationFailException {
		if(passengerNames == null || passengerNames.size() == 0)
			throw new MissingDataException();
		ValidateInput();
		
		List<String> bufferList = new ArrayList<String>();
		bufferList.addAll(passengerNames);
		javax.xml.ws.Holder<List<String>> passengers = new Holder<List<String>>(bufferList);
		javax.xml.ws.Holder<Boolean> success = new Holder<Boolean>();
		try {
			_proxy.book(_number, _depDate, partialBookingAllowed, passengers, success);
			if(!success.value) {
				LOGGER.info("Operation on remote server failed");
				throw new OperationFailException();
			}
			return new HashSet<String>(passengers.value);
		} catch (InvalidFlightInstanceFault_Exception e) {
			LOGGER.info("No such flight instance");
			throw new OperationFailException();
		}
	}

	/**
	 * @category RPC
	 */
	@Override
	public Set<String> getPassengers() throws MissingDataException,
			OperationFailException {
		try {
			ValidateInput();
			
			Set<String> retValueSet = new HashSet<String>();
			List<String> bufferList = _proxy.getPassengerList(_number, _depDate);
			
			retValueSet.addAll(bufferList);
			
			return retValueSet;			
		} catch (InvalidFlightInstanceFault_Exception e) {
			LOGGER.info("No such flight instance");
			throw new OperationFailException();
		}
	}

	/**
	 * @category Validator
	 * @throws MissingDataException
	 */
	private void ValidateInput() throws MissingDataException{
		if(_service == null ||
				_depDate == null ||
				_proxy == null ||
				_number == null) {
			LOGGER.info("Missing parameters");
			throw new MissingDataException();
		}
	}
	
	/**
	 * @category main
	 * @param args
	 */
	public static void main(String[] args){
		FDSBookingClientImpl service = new FDSBookingClientImpl();

		FDSUnbookingClient unbookingClient = new UnbookClientImpl();
		List<FlightInstance> fInstances = unbookingClient.getFlightInstances();
		
		FlightInstance flightInstance = fInstances.get(0);
		service.setFlightNumber(flightInstance.getNumber());
		GregorianCalendar date = new GregorianCalendar();
		date.clear();
		date.set(flightInstance.getDate().getYear().intValue(), flightInstance.getDate().getMonth()-1, flightInstance.getDate().getDay());
		service.setDepartureDate(date);
		
		Set<String> passengersSet = new HashSet<String>();
		for(int i=0; i <1000; i++){
			passengersSet.add("gugu"+i);
		}

		try {	
			List<String> pList = new ArrayList<String>(service.book(passengersSet, false));
			Collections.sort(pList);
			for(String p : pList){
				System.out.println(p);
			}
			pList = new ArrayList<String>(service.getPassengers());
			Collections.sort(pList);
			for(String p : pList){
				System.out.println(p);
			}
		} catch (MissingDataException | OperationFailException e) {
			e.printStackTrace();
		}
	}
}
